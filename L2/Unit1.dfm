object Lab_2: TLab_2
  Left = 182
  Top = 111
  Width = 1083
  Height = 520
  Caption = 'Lab_2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 256
    Top = 16
    Width = 313
    Height = 20
    Caption = 'probability of random numbers (sum=1)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 16
    Width = 737
    Height = 97
    Caption = 'Probability of random numbers (sum=1)'
    TabOrder = 2
    object Grid_table_2: TStringGrid
      Left = 8
      Top = 24
      Width = 721
      Height = 57
      ColCount = 11
      RowCount = 2
      TabOrder = 0
      ColWidths = (
        64
        64
        64
        64
        64
        64
        64
        63
        64
        64
        64)
    end
  end
  object GroupBox1: TGroupBox
    Left = 528
    Top = 120
    Width = 529
    Height = 353
    Caption = 'Rate numbers'
    TabOrder = 0
    object Label2: TLabel
      Left = 233
      Top = 16
      Width = 72
      Height = 25
      Caption = 'PLACE'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 16
      Top = 96
      Width = 7
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Grid_table: TStringGrid
      Left = 48
      Top = 48
      Width = 465
      Height = 281
      ColCount = 7
      RowCount = 11
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 120
    Width = 505
    Height = 353
    Caption = 'Histogram samples of return'
    TabOrder = 1
    object Chart1: TChart
      Left = 24
      Top = 24
      Width = 457
      Height = 321
      AllowPanning = pmNone
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      BottomWall.Color = clBlack
      LeftWall.Color = 33023
      MarginBottom = 10
      MarginLeft = 0
      MarginRight = 5
      MarginTop = 0
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.Visible = False
      LeftAxis.MinorTickLength = 3
      LeftAxis.TickLength = 3
      Legend.Visible = False
      View3D = False
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 64
        Top = 296
        Width = 351
        Height = 13
        Caption = 
          '1          2          3           4           5           6     ' +
          '      7           8          9          10'
      end
      object Series1: TBarSeries
        Marks.ArrowLength = 20
        Marks.Style = smsValue
        Marks.Visible = True
        SeriesColor = 4227072
        MultiBar = mbNone
        XValues.DateTime = False
        XValues.Name = 'X'
        XValues.Multiplier = 1
        XValues.Order = loAscending
        YValues.DateTime = False
        YValues.Name = 'Bar'
        YValues.Multiplier = 1
        YValues.Order = loNone
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 904
    Top = 16
    Width = 153
    Height = 97
    TabOrder = 3
    object Lab_name: TLabel
      Left = 16
      Top = 16
      Width = 124
      Height = 13
      Caption = 'Lab 2. Sample generation.'
    end
    object Label5: TLabel
      Left = 40
      Top = 48
      Width = 67
      Height = 13
      Caption = 'Yartsev Anton'
    end
    object Label6: TLabel
      Left = 40
      Top = 64
      Width = 69
      Height = 13
      Caption = 'Sibsutis. 2016.'
    end
  end
  object GroupBox5: TGroupBox
    Left = 760
    Top = 16
    Width = 137
    Height = 97
    Caption = 'Controls'
    TabOrder = 4
    object Button_generate: TButton
      Left = 30
      Top = 24
      Width = 75
      Height = 25
      Caption = 'generate'
      TabOrder = 0
      OnClick = Button_generateClick
    end
  end
end
