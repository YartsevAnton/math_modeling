//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Series.hpp>
//---------------------------------------------------------------------------
class TLab_2 : public TForm
{
__published:	// IDE-managed Components
        TButton *Button_generate;
        TChart *Chart1;
        TLabel *Label3;
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TGroupBox *GroupBox4;
        TLabel *Lab_name;
        TLabel *Label5;
        TLabel *Label6;
        TGroupBox *GroupBox5;
        TChart *Chart2;
        TBarSeries *Series2;
        TRadioButton *RadioButton1;
        TRadioButton *RadioButton2;
        TLineSeries *Series1;
        void __fastcall Button_generateClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TLab_2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLab_2 *Lab_2;
//---------------------------------------------------------------------------
#endif
