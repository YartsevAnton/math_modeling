object Lab_2: TLab_2
  Left = 11
  Top = 164
  Width = 1343
  Height = 503
  Caption = 'Lab_3'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 1024
    Top = 104
    Width = 297
    Height = 353
    TabOrder = 0
    object Label3: TLabel
      Left = 16
      Top = 96
      Width = 7
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -20
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Chart2: TChart
      Left = 8
      Top = 16
      Width = 281
      Height = 321
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      MarginBottom = 10
      MarginTop = 0
      Title.Text.Strings = (
        '[Histogram distribution matrices]')
      Legend.Visible = False
      View3D = False
      TabOrder = 0
      object Series2: TBarSeries
        Marks.ArrowLength = 20
        Marks.Visible = False
        SeriesColor = clRed
        XValues.DateTime = False
        XValues.Name = 'X'
        XValues.Multiplier = 1
        XValues.Order = loAscending
        YValues.DateTime = False
        YValues.Name = 'Bar'
        YValues.Multiplier = 1
        YValues.Order = loNone
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 0
    Width = 1009
    Height = 457
    TabOrder = 1
    object Chart1: TChart
      Left = 8
      Top = 16
      Width = 993
      Height = 433
      AllowPanning = pmNone
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      BottomWall.Color = clBlack
      LeftWall.Color = 33023
      MarginBottom = 10
      MarginTop = 0
      Title.Text.Strings = (
        '[Behavor random variable]')
      LeftAxis.MinorTickLength = 3
      LeftAxis.TickLength = 3
      Legend.Visible = False
      View3D = False
      BevelOuter = bvLowered
      TabOrder = 0
      object Series1: TLineSeries
        Marks.ArrowLength = 20
        Marks.Style = smsValue
        Marks.Visible = False
        SeriesColor = clRed
        LinePen.Color = clRed
        LinePen.Width = 2
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.Visible = False
        XValues.DateTime = False
        XValues.Name = 'X'
        XValues.Multiplier = 1
        XValues.Order = loAscending
        YValues.DateTime = False
        YValues.Name = 'Y'
        YValues.Multiplier = 1
        YValues.Order = loNone
      end
    end
  end
  object GroupBox4: TGroupBox
    Left = 1168
    Top = 0
    Width = 153
    Height = 97
    TabOrder = 2
    object Lab_name: TLabel
      Left = 32
      Top = 16
      Width = 91
      Height = 13
      Caption = 'Lab 3. Markov link.'
    end
    object Label5: TLabel
      Left = 40
      Top = 48
      Width = 67
      Height = 13
      Caption = 'Yartsev Anton'
    end
    object Label6: TLabel
      Left = 40
      Top = 64
      Width = 69
      Height = 13
      Caption = 'Sibsutis. 2016.'
    end
  end
  object GroupBox5: TGroupBox
    Left = 1024
    Top = 0
    Width = 137
    Height = 97
    Caption = 'Controls'
    TabOrder = 3
    object Button_generate: TButton
      Left = 30
      Top = 24
      Width = 75
      Height = 25
      Caption = 'generate'
      TabOrder = 0
      OnClick = Button_generateClick
    end
    object RadioButton1: TRadioButton
      Left = 40
      Top = 56
      Width = 73
      Height = 17
      Caption = 'Matrix A'
      Checked = True
      TabOrder = 1
      TabStop = True
    end
    object RadioButton2: TRadioButton
      Left = 40
      Top = 72
      Width = 81
      Height = 17
      Caption = 'Matrix B'
      TabOrder = 2
    end
  end
end
