#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <Math.hpp>
#define NumLen 10 // number length
#define SizeSample 6  // size of sample
#define QuaSample 10 // quantity of sample
#pragma package(smart_init)
#pragma resource "*.dfm"
TLab_2 *Lab_2;
#define Left 0
#define Right 1

//dont_touch!---------------------------------------------------------------
__fastcall TLab_2::TLab_2(TComponent* Owner)
        : TForm(Owner)
{
}
int i, r, k, c, j, l, p, grid_rows=1, grid_cows=1;
double test_sum, b, sum, r2;
double *num = (double*) malloc(NumLen * sizeof(double));
double *num_p = (double*) malloc(NumLen * sizeof(double));
int *gist = (int*) malloc(NumLen* sizeof(double));
double *S = (double*) malloc(NumLen * sizeof(double));
bool h;
//---------------------------------------------------------------------------

void __fastcall TLab_2::Button_generateClick(TObject *Sender)
{

	srand(time(NULL));
	Series1->Clear();

        for (i = 0; i < NumLen; i++)
        {
                b = static_cast<double>(1) / static_cast<double> (NumLen - i);
                num[i] = b;
                sum += b;
        }

        for(i = 0; i < NumLen; ++i)
        {
                num_p[i] = (1 / sum) * num[i];
                Grid_table_2->Cells[i+1][1]=FloatToStr(SimpleRoundTo(num_p[i],-6));
                test_sum += num_p[i];
        }

        for(i = 0; i < SizeSample; ++i) //on returns
        {
                r = 0 + rand() % NumLen;
        }

        S[0] = num_p[0];

        for (i = 1; i < NumLen; ++i)
        {
                S[i] = S[i - 1] + num_p[i];
        }

        for (i = 0; i < 100000; ++i)
        {
                r2 = (float) rand() / RAND_MAX;
                for(l = 0; l < NumLen; ++l)
                {
                        if (num_p[l] > r2)
                        {
                                gist[l]++;
                        }
                }
        }

        for (i = 0; i < NumLen; i++)
        {
        Series1->Add(gist[i],gist[i],clRed);
        }

        for(k = 0; k < QuaSample; ++k)	//no returns
        {
                for(i = 0; i < SizeSample; i++)
                {
                        do
                        {
                                h = false;
                                c = 0 + rand()% NumLen;
                                for (j = 0; j < i; j++)
                                {
                                        if(c == num_p[j])
                                        h = true;
                                }
                        }
                        while(h);
                        num_p[i] = c;
                }

                grid_rows=1;
                for(i = 0; i < SizeSample; i++)
                {
                        p = num_p[i];
                        Grid_table->Cells[grid_rows][grid_cows]=p; //=p
                        grid_rows++;
                }
                grid_cows++;
        }
        free(num);
        free(num_p);
        free(gist);
        free(S);
}

//at form create apply misc changes
void __fastcall TLab_2::FormCreate(TObject *Sender)
{
        Grid_table_2->Cells[0][0]="i";
        Grid_table_2->Cells[0][1]="Pi";
        Label3->WordWrap = true;
    	Label3->Caption = "N\r\nU\r\nM\r\nB\rE\rR";

        for(int xx = 0; xx < 10; xx++)
        {
                Grid_table->Cells[0][xx+1]=xx;
                Grid_table_2->Cells[xx+1][0]=xx;
        }

        for(int xx = 0; xx < 6; xx++)
        {
                Grid_table->Cells[xx+1][0]=xx;
        }
}

