//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Series.hpp>
//---------------------------------------------------------------------------
class TLab_2 : public TForm
{
__published:	// IDE-managed Components
        TButton *Button_generate;
        TStringGrid *Grid_table;
        TChart *Chart1;
        TBarSeries *Series1;
        TLabel *Label1;
        TStringGrid *Grid_table_2;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TGroupBox *GroupBox3;
        TGroupBox *GroupBox4;
        TLabel *Lab_name;
        TLabel *Label5;
        TLabel *Label6;
        TGroupBox *GroupBox5;
        void __fastcall Button_generateClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TLab_2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLab_2 *Lab_2;
//---------------------------------------------------------------------------
#endif
