#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <Math.hpp>

#pragma package(smart_init)
#pragma resource "*.dfm"
TLab_2 *Lab_2;

#define GENERATED_NUMBER 10000000
#define MAX_MATRIX 10

int flag;

double matrix_A[MAX_MATRIX][MAX_MATRIX] =
 {
  0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.3,
  0.2, 0.3, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.3, 0.5, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.2, 0.3, 0.5, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.2, 0.3, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.2, 0.3, 0.5, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.2, 0.3, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.3, 0.5, 0.0, 0.0,
  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.3, 0.5,
  0.3, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2,
 };

double matrix_B[MAX_MATRIX][MAX_MATRIX] =
{
 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5,
 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5,
 0.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0,
 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0,
 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0, 0.0,
 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.0,
 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.0, 0.0,
};

//dont_touch!---------------------------------------------------------------
__fastcall TLab_2::TLab_2(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TLab_2::Button_generateClick(TObject *Sender)
{
  int history_x[MAX_MATRIX];
  memset(history_x, 0, sizeof(int) * MAX_MATRIX);
  srand(time(NULL));

	Series1->Clear();
  Series2->Clear();

    if(RadioButton1->Checked) //select matrix A
    {
      flag=0;
    }

    if(RadioButton2->Checked) //select matrix B
    {
      flag=1;
    }

   double matrix_probability[MAX_MATRIX][MAX_MATRIX], array_ksi[500], ksi;
   for(int i = 0; i < MAX_MATRIX; ++i) 
   {
      if (flag==0)
      {
        matrix_probability[i][0] = matrix_A[i][0];
      }
       
      else
      {
        matrix_probability[i][0] = matrix_B[i][0];
      }

      for (int j = 1; j < MAX_MATRIX; ++j) 
      {
        if (flag==0)
        {
          matrix_probability[i][j] = matrix_A[i][j] + matrix_probability[i][j - 1];
        }
        else
        {
          matrix_probability[i][j] = matrix_B[i][j] + matrix_probability[i][j - 1];
        }
      }
   }

   for (int z = 0, current_event = 0; z < GENERATED_NUMBER; ++z) 
   {
      double tmp = ((double) rand() / RAND_MAX);
      for (int i = 0, current_x; i < MAX_MATRIX; ++i) 
      {
        if (tmp < matrix_probability[current_event][i]) 
        {
          current_event = i;
          do
          {
            ksi = ((double) rand() / RAND_MAX);

            if (ksi==0) //to avoid log(0)
            {
              ksi++;
            }
            else
            {
              ksi = -1.0 * (log(ksi));
            }

            current_x = (int)(ksi / 0.7);
          } 
          while (current_x >= 10);
          ++history_x[current_x];
          break;
        }
      }
        if (z < 500) 
        {
          array_ksi[z] = ksi;
        }
    }

   for (int i = 0; i < MAX_MATRIX; ++i) 
   {
      Series2->Add(history_x[i],i+1,clRed); //draw a behavor
   }

   for (int i = 0; i < 500; ++i) 
   {
      Series1->Add(array_ksi[i],i+1,clRed); //draw histogram
   }
}

