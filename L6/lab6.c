#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//for color-----------------
#define RESET   "\033[0m"
#define RED     "\033[1;31m"
#define YELLOW  "\033[1;44m"
#define GREEN   "\033[1;42m"
//--------------------------

//rand macros (easy random)------------
#define random ((float)rand()/RAND_MAX)
//-------------------------------------
#define events 1000

//event struct.
struct event 
{  
    float time;   
    void (*proc)();   
    struct event *next;  
    struct event *prev;
};

//N struct.
typedef struct 
{   
    int data;        
    int sum;          
    int c_change;    
    int c_zero;        
                                                
} var;  //name for N struct.

var N; 

float now = 0; //current model. time

struct event *calendar = NULL;  //initializ. calendar (need link on any event!)

FILE *f;

void E1();
void E2();

//add task to calebdar-------------------------------------------------------------------
void push(struct event *new)
{
    struct event *event = calendar;
    if (calendar == NULL) //if calendar empty
    { 
        new->next = NULL;  //write [new] in start
        new->prev = NULL;
        calendar = new;
        return;
    } 

    while (event != NULL) { //if calendar not empty
        if (event->time > new->time)  //если время найденного события стало больше, чем время добавляемого 
        {
            new->next = event; //add [new] before [event] 
            new->prev = event->prev;
            if (event->prev)
                event->prev->next = new;
            else
                calendar = new;
            event->prev = new;
            return;
        }

        if (event->next == NULL) { //if calendar end
            new->next = NULL; // добавление new после event
            new->prev = event;
            event->next = new;
            return;
        }
        event = event->next; // go to next event
    }
}
//--------------------------------------------------------------------------

//take event from calendar--------------------------------------------------
struct event *pop()
{
    struct event *event = calendar;
    event->next->prev = NULL;
    calendar = event->next;
    return event;
}
//----------------------------------------------------------------------------


//Planned func. Input: event & time.------------------------------------------
//Creates new event in calendar
void schedule(void (*proc)(), float time)
{
    struct event *new;
    if (proc == E1)
    	printf("%sE1 PLANNED on time [%.5f]%s",YELLOW, time, RESET);
    else
    	printf("%sE2 PLANNED on time [%.5f]%s",GREEN, time, RESET);
    new = malloc(sizeof(struct event));    /* выделяем память под новое */
    new->time = time;   /* время */
    new->proc = proc;   /* какая процедура */
    push(new);          /* добавить в календарь */
}
//-----------------------------------------------------------------------------


//refresh VAR type-------------------------------------------------------------
//Input: refreshing variable & new value for variable
void update_var(var *v, int value)
{
    v->data = value;
    v->sum += value;   
    v->c_change++;
    if (v->data == 0) {
        v->c_zero++;
        fprintf(f, "%f\n", now); /* записываем в файл время обращения в 0 */
    }
}
//------------------------------------------------------------------------------


//обнуляем статистику-----------------------------------------------------------
void init_var(var *v)
{
    v->data = 0;
    v->sum = 0;
    v->c_change = 0;
    v->c_zero = 0;
}
//------------------------------------------------------------------------------


//print stat--------------------------------------------------------------------
void statistics(var *v, char *msg)
{
    printf("%s\n", msg); 
    printf("\tAVG  = %.3f\n", (float)v->sum/v->c_change);
    printf("\tZERO = %d\n", v->c_zero);
}
//------------------------------------------------------------------------------


//start simulation--------------------------------------------------------------
//take [event 1, 2, n] from calendar and execute. (limit 1000 events)
int startsim()
{
    int i = 0;
    struct event *cur; //current executable event
    while (calendar != NULL && i++ < events) 
    { 
        cur = pop(); // pop first event from calendar
       // printf("%sCurrent time%s [%.5f] %sSTART SIMULATION%s ", GREEN, RESET, now, GREEN, RESET);
        now = cur->time; /* теперь текущее время изменилось на время текущего события*/
        cur->proc(); /* запускаем процедуру, соответствующую событию */
        free(cur);   /* освобождаем память события */
    }

    if (calendar == NULL) // if calendar empty
        return 1;
    return 2;  // if limit (1000 events)
}

void E1()
{
    printf("%sE1 START: current time [%.5f]%s\n", YELLOW, RESET, now);
    update_var(&N, N.data+1);
    schedule(E1, now+random);
}

void E2()
{
    printf("%sE2 START: current time [%.5f]%s\n", GREEN, RESET, now);
    update_var(&N, N.data-1);
    schedule(E2, now+random);
}

/*-------------------------------------------------------------------------------------------------*/
int main()
{
    srand(time(NULL)); 
    f = fopen("output.txt", "w");
    init_var(&N); //clear N

    //planned E1 & E2 on random time
    schedule(E1, now+random); 
    schedule(E2, now+random);

    //start simulation..
    switch (startsim()) { 
        case 1: printf("%sEXIT. calendar empty%s\n",RED, RESET); break; //if calendar = null
        case 2: printf("%sEXIT. event limit (%d)%s\n",RED, events, RESET); break; //if event = 1000
    }
    statistics(&N, "N statistics:");
    fclose(f);
    return 0;
}
/*-------------------------------------------------------------------------------------------------*/
