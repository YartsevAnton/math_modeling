//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include <stdlib.h>
#pragma hdrstop

#include "Unit1.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
int Count_Series=99;

//dont_touch!---------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner){
}
//--------------------------------------------------------------------------

//Generation tree with degree limit
int __fastcall TForm1::Gen3()
{
    int pointX[100];
    int pointY[100];
    int lim[100];
    int i, n, l, r, max,temp;
    int flag=0;

    Memo3->Clear();
    max=n=StrToInt(Edit1->Text);

    //Setup matrix
    StringGrid1->ColCount=max+1;
    StringGrid1->RowCount=max+1;
    for (int i = 0; i < StringGrid1->ColCount; i++)
    for (int j = 0; j < StringGrid1->RowCount; j++)
    StringGrid1->Cells[i][j] = "0";
    for(int xx = 0; xx < max; xx++)
        {
                StringGrid1->Cells[0][xx+1]=xx+1;
                StringGrid1->Cells[xx+1][0]=xx+1;
        }

    //clear graph
    for(i=0; i<Count_Series; i++)Chart1->Series[i]->Clear();

    for(i=0; i<n; i++)
    {
        pointX[i]=i+1;
        lim[i]=StrToInt(Edit2->Text); //lim
    }


    for(i=0; i<n; i++)pointY[i]=10-random(20);

    l=random(n);
    temp=pointX[l];
    pointX[l]=pointX[n-1];
    pointX[n-1]=temp;

    temp=pointY[l];
    pointY[l]=pointY[n-1];
    pointY[n-1]=temp;

    temp=lim[l];
    lim[l]=lim[n-1];
    lim[n-1]=temp;

    for(i=0;i<(max-1);i++)
    {
        --n;
        do{l=random(n);
    }

    while(lim[l]==0);
		do
		{
			r=random(max-n)+n;
        }
        while(lim[r]==0);

        //flag for save main vertex
        if (flag==0)
        {
                Memo3->Lines->Add("Main vertex "+IntToStr(pointX[l]));
                StringGrid1->Cells[pointX[l]] [1]="1";
                flag=1;
        }

        //to log
        Memo3->Lines->Add("Edge "+IntToStr(i+1)+":  "+IntToStr(pointX[l])+"-"+IntToStr(pointX[r]));

        //to matrix
        StringGrid1->Cells[pointX[l]] [i+2]="1";
        StringGrid1->Cells[pointX[r]] [i+2]="1";

        //to graph
        Chart1->Series[i]->AddXY(pointX[r], pointY[r]);
        Chart1->Series[i]->AddXY(pointX[l], pointY[l]);

        --lim[l];
        --lim[r];

        temp=pointX[l];
        pointX[l]=pointX[n-1];
        pointX[n-1]=temp;

        temp=pointY[l];
        pointY[l]=pointY[n-1];
        pointY[n-1]=temp;

        temp=lim[l];
        lim[l]=lim[n-1];
        lim[n-1]=temp;
    }

      StringGrid1->Cells[pointX[0]] [0]="1";

    /*
      //last edge, for full graph
      Chart1->Series[i]->AddXY(pointX[1], pointY[1]);
      Chart1->Series[i]->AddXY(pointX[l], pointY[l]);

      StringGrid1->Cells[pointX[1]] [i+1]="1";
      StringGrid1->Cells[pointX[l]] [i+1]="1";

      Memo3->Lines->Add("Edge "+IntToStr(i+1)+":  "+IntToStr(pointX[1])+"-"+IntToStr(pointX[l]));
    */
    return 1;
}

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        Gen3(); //call generate
}

void __fastcall TForm1::FormCreate(TObject *Sender){
        Randomize();
}

void __fastcall TForm1::Edit1Exit(TObject *Sender)
{
    if(StrToInt(Edit1->Text)<2)Edit1->Text=2;       
    if(StrToInt(Edit1->Text)>100)Edit1->Text=100;

}

void __fastcall TForm1::Edit2Exit(TObject *Sender){
    if(StrToInt(Edit2->Text)>=StrToInt(Edit1->Text))Edit2->Text=StrToInt(Edit1->Text)-1;
    if(StrToInt(Edit2->Text)<1)Edit2->Text=2;
}

