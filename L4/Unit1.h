//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <ComCtrls.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TChart *Chart1;
    TEdit *Edit1;
        TButton *Button1;
        TLineSeries *Series1;
        TLineSeries *Series2;
        TLineSeries *Series3;
        TLineSeries *Series4;
        TLineSeries *Series5;
        TLineSeries *Series6;
        TLineSeries *Series7;
        TLineSeries *Series8;
        TLineSeries *Series9;
        TLineSeries *Series10;
        TLineSeries *Series11;
        TLineSeries *Series12;
        TLineSeries *Series13;
        TLineSeries *Series14;
        TLineSeries *Series15;
        TLineSeries *Series16;
        TLineSeries *Series17;
        TLineSeries *Series18;
        TLineSeries *Series19;
        TLineSeries *Series20;
        TLineSeries *Series21;
        TLineSeries *Series22;
        TLineSeries *Series23;
        TLineSeries *Series24;
        TLineSeries *Series25;
        TLineSeries *Series26;
        TLineSeries *Series27;
        TLineSeries *Series28;
        TLineSeries *Series29;
        TLineSeries *Series30;
        TLineSeries *Series31;
        TLineSeries *Series32;
        TLineSeries *Series33;
        TLineSeries *Series34;
        TLineSeries *Series35;
        TLineSeries *Series36;
        TLineSeries *Series37;
        TLineSeries *Series38;
        TLineSeries *Series39;
        TLineSeries *Series40;
        TLineSeries *Series41;
        TLineSeries *Series42;
        TLineSeries *Series43;
        TLineSeries *Series44;
        TLineSeries *Series45;
        TLineSeries *Series46;
        TLineSeries *Series47;
        TLineSeries *Series48;
        TLineSeries *Series49;
        TLineSeries *Series50;
        TLineSeries *Series51;
        TLineSeries *Series52;
        TLineSeries *Series53;
        TLineSeries *Series54;
        TLineSeries *Series55;
        TLineSeries *Series56;
        TLineSeries *Series57;
        TLineSeries *Series58;
        TLineSeries *Series59;
        TLineSeries *Series60;
        TLineSeries *Series61;
        TLineSeries *Series62;
        TLineSeries *Series63;
        TLineSeries *Series64;
        TLineSeries *Series65;
        TLineSeries *Series66;
        TLineSeries *Series67;
        TLineSeries *Series68;
        TLineSeries *Series69;
        TLineSeries *Series70;
        TLineSeries *Series71;
        TLineSeries *Series72;
        TEdit *Edit2;
        TMemo *Memo3;
        TLineSeries *Series73;
        TLineSeries *Series74;
        TLineSeries *Series75;
        TLineSeries *Series76;
        TLineSeries *Series77;
        TLineSeries *Series78;
        TLineSeries *Series79;
        TLineSeries *Series80;
        TLineSeries *Series81;
        TLineSeries *Series82;
        TLineSeries *Series83;
        TLineSeries *Series84;
        TLineSeries *Series85;
        TLineSeries *Series86;
        TLineSeries *Series87;
        TLineSeries *Series88;
        TLineSeries *Series89;
        TLineSeries *Series90;
        TLineSeries *Series91;
        TLineSeries *Series92;
        TLineSeries *Series93;
        TLineSeries *Series94;
        TLineSeries *Series95;
        TLineSeries *Series96;
        TLineSeries *Series97;
        TLineSeries *Series98;
        TLineSeries *Series99;
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TLabel *Label1;
        TLabel *Label2;
        TGroupBox *GroupBox4;
        TLabel *Lab_name;
        TLabel *Label5;
        TLabel *Label6;
        TStringGrid *StringGrid1;
        TGroupBox *GroupBox3;
        TGroupBox *GroupBox5;
        int __fastcall Gen3();
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Edit1Exit(TObject *Sender);
    void __fastcall Edit2Exit(TObject *Sender);
private: 	// User declarations
public:	  	// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
